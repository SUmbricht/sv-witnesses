from pycparser import c_parser, c_generator, c_ast
from collections import Counter


def getAst(text):
    parser = c_parser.CParser()
    return parser.parse(text, filename="<none>")


class NondetCallCounter(c_ast.NodeVisitor):
    def __init__(self):
        self.nondet_calls = Counter()

    def visit_FuncCall(self, node):
        if "__VERIFIER_nondet" in node.name.name:
            line = node.coord.line
            self.nondet_calls[line] += 1
        super().generic_visit(node)

    def max_nondet_calls(self):
        return max(self.nondet_calls.values(), default=0)


def analyze_program(source):
    result = dict()
    ast = getAst(source)
    ndcc = NondetCallCounter()
    ndcc.visit(ast)
    result["max_nondet_calls_per_line"] = ndcc.max_nondet_calls()
    return result
